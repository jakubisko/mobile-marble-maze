import React, { useRef } from 'react';
import { useFrame } from 'react-three-fiber';
import { useStore } from '../model/store';
import { Floor } from './Floor';
import { Balls } from './Balls';
import { mazeWidth, mazeHeight } from '../model/levelDefinition';
import { useDeviceOrientation } from './useDeviceOrientation';
import { PointLight } from 'three';
import { DeviceMotionState } from './useDeviceMotion';
import { Screen } from '../model/types';
import { Walls } from './Walls';

function tangent(x: number): number {
  return (x <= -90 || x >= 90) ? 10000 : Math.tan(x * Math.PI / 180);
}

export function GameObjects() {
  const pointLight = useRef<PointLight>();

  const mutable = useStore(state => state.mutable);
  const physicsEngine = useStore(state => state.physicsEngine);
  const screen = useStore(state => state.screen);
  const showScreen = useStore(state => state.showScreen);
  const increaseLevelTime = useStore(state => state.increaseLevelTime);
  const updateBestTimes = useStore(state => state.updateBestTimes);

  const simulatedDeviceOrientation = useStore(state => state.simulatedDeviceOrientation);
  const readDeviceOrientation = useDeviceOrientation();
  const deviceOrientation = simulatedDeviceOrientation ?? readDeviceOrientation;
  const deviceMotion: DeviceMotionState = { x: 0, y: 0, z: 0 };

  // Implemented but deliberately not used. This has little effect on normal gameplay, but makes the game unplayable in the moving vehicles.
  // const deviceMotion = useDeviceMotion();

  // Once per frame...
  useFrame(() => {

    // Move the light. Light is always responsive; even when the gameplay is stopped.
    pointLight.current!.position.x = -tangent(deviceOrientation.beta) * mazeWidth;
    pointLight.current!.position.y = -tangent(deviceOrientation.gamma) * mazeWidth;

    // Balls are allowed to move only when the gameplay is not stopped.
    if (screen !== Screen.Playing && screen !== Screen.Intro && screen !== Screen.Menu) {
      return;
    }

    // Calculate time from the last drawing in seconds. Delta may be large if the tab was minimized, so there has to be an upper limit.
    const newTime = Date.now();
    const deltaTime = Math.min(newTime - mutable.lastTime, 100) / 1000;
    if (deltaTime === 0) {
      return;
    }
    increaseLevelTime(newTime - mutable.lastTime);
    mutable.lastTime = newTime;

    // Move the balls. 
    physicsEngine.update(deltaTime, deviceOrientation, deviceMotion);

    // Check the end of the level.
    if (physicsEngine.numberOfBallsInHoles(false) > 0) {
      showScreen(Screen.LevelLost);
    } else if (physicsEngine.numberOfBallsInHoles(true) === mutable.balls.length) {
      updateBestTimes();
      showScreen(Screen.LevelWon);
    } else if (deviceOrientation.beta <= -90 || deviceOrientation.beta >= 90 || deviceOrientation.gamma <= -90 || deviceOrientation.gamma >= 90) {
      // The mobile is upside down.
      showScreen(Screen.LevelLost);
    }
  });

  return (
    <group>
      <ambientLight />
      <pointLight
        ref={pointLight}
        intensity={0.5}
        position={[0, 0, 20]}
        castShadow
        shadow-mapSize-width={1024}
        shadow-mapSize-height={1024}
        shadow-camera-far={1000}
        shadow-camera-left={-10}
        shadow-camera-right={10}
        shadow-camera-top={10}
        shadow-camera-bottom={-10}
      />
      <group position={[(1 - mazeWidth) / 2, (1 - mazeHeight) / 2, 0]}>
        <Floor />
        <Walls />
        <Balls />
        {simulatedDeviceOrientation !== undefined && (
          <mesh position={[(mazeWidth - 1) / 2, (mazeHeight - 1) / 2, 1]}>
            <ringBufferGeometry attach="geometry" args={[0.1, 0.2, 32]} />
            <meshStandardMaterial attach="material" color="red" />
          </mesh>
        )}
      </group>
    </group>
  );
}
