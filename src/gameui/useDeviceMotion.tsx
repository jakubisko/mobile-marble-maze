import { useState, useEffect } from 'react';

export interface DeviceMotionState {
  x: number;
  y: number;
  z: number;
}

// Hook
export function useDeviceMotion() {
  const [deviceMotion, setDeviceMotion] = useState<DeviceMotionState>({
    x: 0,
    y: 0,
    z: 0
  });

  useEffect(() => {
    // Handler to call on window motion change
    function handleMotionChange(event: DeviceMotionEvent) {
      // Set to state
      setDeviceMotion({
        x: event?.acceleration?.x ?? 0,
        y: event?.acceleration?.y ?? 0,
        z: event?.acceleration?.z ?? 0
      });
    }

    // Add event listener
    window.addEventListener("devicemotion", handleMotionChange, true);

    // Remove event listener on cleanup
    return () => window.removeEventListener("devicemotion", handleMotionChange, true);
  }, []); // Empty array ensures that effect is only run on mount

  return deviceMotion;
}
