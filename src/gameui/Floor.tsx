import React, { useEffect, useRef } from 'react';
import { useLoader } from 'react-three-fiber';
import woodTextureURL from './wood.jpg';
import { TextureLoader, PlaneGeometry, MeshBasicMaterial, Geometry, Material } from 'three';
import { mazeWidth, mazeHeight } from '../model/levelDefinition';
import { useStore } from '../model/store';
import { holeRadius } from '../model/PhysicsEngine';

const finishCells = 4; // Number of squares on each side of the finish flag. Has to be even.

export function Floor() {
  const woodTexture = useLoader(TextureLoader, woodTextureURL);
  const deskGeometry = useStore(state => state.deskGeometry);

  // Finishes
  const finishes = useStore(state => state.mutable.finishes);
  const finishGeometry = useRef<Geometry>();
  const finishMaterial = useRef<Material>();

  useEffect(() => {
    finishGeometry.current = new PlaneGeometry(2 * holeRadius, 2 * holeRadius, finishCells, finishCells);
    finishGeometry.current.faces.forEach((face, i) => {
      if ((i + (Math.floor(i / (finishCells * 2)) % 2 * 2)) % 4 < 2) {
        face.color.setRGB(0, 0, 0);
      }
    });

    finishMaterial.current = new MeshBasicMaterial({ color: 0xffffff, vertexColors: true });

    return () => {
      finishGeometry.current!.dispose();
      finishMaterial.current!.dispose();
    };
  }, []);

  return (
    <group>
      <mesh
        position={[(mazeWidth - 1) / 2, (mazeHeight - 1) / 2, -1]}
        geometry={deskGeometry}
        castShadow
        receiveShadow
      >
        <meshPhongMaterial attach="material" map={woodTexture} color="rgb(120, 120, 120)" />
      </mesh>

      {finishes.map((finish, id) => (
        <mesh
          key={id}
          position={[finish.x, finish.y, -1.999]}
          geometry={finishGeometry.current}
          material={finishMaterial.current}
          receiveShadow
        />
      ))}

      <mesh
        position={[(mazeWidth - 1) / 2, (mazeHeight - 1) / 2, -2]}
        receiveShadow
      >
        <planeGeometry attach="geometry" args={[mazeWidth, mazeHeight]} />
        <meshPhongMaterial attach="material" map={woodTexture} color="rgb(40, 40, 40)" />
      </mesh>
    </group>
  );
}
