import { useState, useEffect } from 'react';

export interface WindowSizeState {
  width: number;
  height: number;
}

// Hook
export function useWindowSize() {
  const [windowSize, setWindowSize] = useState<WindowSizeState>({
    width: 0,
    height: 0
  });

  useEffect(() => {
    // Handler to call on window size change
    function handleSizeChange() {
      // Set to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight
      });
    }

    // Add event listener
    window.addEventListener("resize", handleSizeChange, true);

    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleSizeChange, true);
  }, []); // Empty array ensures that effect is only run on mount

  return windowSize;
}
