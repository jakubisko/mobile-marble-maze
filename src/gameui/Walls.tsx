import React from 'react';
import { useLoader } from 'react-three-fiber';
import woodTextureURL from './wood.jpg';
import { TextureLoader } from 'three';
import { mazeWidth, mazeHeight } from '../model/levelDefinition';
import { useStore } from '../model/store';

export function Walls() {
  const woodTexture = useLoader(TextureLoader, woodTextureURL);
  const wallGeometry = useStore(state => state.wallGeometry);

  return (
    <mesh
      position={[(mazeWidth - 1) / 2, (mazeHeight - 1) / 2, 0]}
      geometry={wallGeometry}
      castShadow
      receiveShadow
    >
      <meshPhongMaterial attach="material" map={woodTexture} color="rgb(180, 160, 160)" />
    </mesh>
  );
}
