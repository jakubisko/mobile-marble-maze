import { useState, useEffect } from 'react';

export interface DeviceOrientationState {
  alpha: number;
  beta: number;
  gamma: number;
}

// Hook
export function useDeviceOrientation() {
  const [deviceOrientation, setDeviceOrientation] = useState<DeviceOrientationState>({
    alpha: 0,
    beta: 0,
    gamma: 0
  });

  useEffect(() => {
    // Handler to call on window orientation change
    function handleOrientationChange(event: DeviceOrientationEvent) {
      // Set to state
      setDeviceOrientation({
        alpha: event.alpha ?? 0,
        beta: event.beta ?? 0,
        gamma: event.gamma ?? 0
      });
    }

    // Add event listener
    window.addEventListener("deviceorientation", handleOrientationChange, true);

    // Remove event listener on cleanup
    return () => window.removeEventListener("deviceorientation", handleOrientationChange, true);
  }, []); // Empty array ensures that effect is only run on mount

  return deviceOrientation;
}
