import React, { Suspense } from 'react';
import { Canvas } from 'react-three-fiber';
import { GameObjects } from './GameObjects';
import { useStore } from '../model/store';
import { useWindowSize } from './useWindowSize';

function easingFunc(x: number): number {
  return Math.sign(x) * Math.pow(Math.abs(x), 1.5);
}

export function GameCanvas() {
  const setSimulatedDeviceOrientation = useStore(store => store.setSimulatedDeviceOrientation);
  const loadLevel = useStore(store => store.loadLevel);

  const windowSize = useWindowSize();

  return (
    <Canvas
      concurrent
      noEvents // Don't generate three.js events such as when an object is clicked. Standard html events will be generated.
      camera={{
        fov: 25,
        position: [0, 0, 64],
        near: 63,
        far: 67
      }}
      shadowMap
      onCreated={({ gl }) => { gl.setClearColor('black'); loadLevel(0); }}
      onMouseMove={e => (e.buttons === 1) && setSimulatedDeviceOrientation({
        alpha: 0,
        beta: 89.9 * easingFunc(2 * e.clientX / windowSize.width - 1),
        gamma: 89.9 * easingFunc(1 - 2 * e.clientY / windowSize.height),
      })}
    >
      <Suspense fallback={null}>
        <GameObjects />
      </Suspense>
    </Canvas>
  );
}
