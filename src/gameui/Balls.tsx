import React, { useRef } from 'react';
import { useFrame, useLoader } from 'react-three-fiber';
import { Mesh, TextureLoader, Texture } from 'three';
import { useStore } from '../model/store';
import { ballRadius } from '../model/PhysicsEngine';
import metalURL from './metal.jpg';
import { BallData } from '../model/types';

export function Balls() {
  const ballTexture = useLoader(TextureLoader, metalURL);
  const mutable = useStore(state => state.mutable);
  return <>{mutable.balls.map((ball, id) => <Ball key={id} ball={ball} ballTexture={ballTexture} />)}</>;
}

function Ball({ ball, ballTexture }: { ball: BallData, ballTexture: Texture }) {
  const mesh = useRef<Mesh>();

  useFrame(() => {
    mesh.current!.position.x = ball.x;
    mesh.current!.position.y = ball.y;
    mesh.current!.position.z = ball.z;
  });

  return (
    <mesh ref={mesh} position={[0, 0, 0]} castShadow receiveShadow>
      <sphereBufferGeometry attach="geometry" args={[ballRadius, 16, 16]} />
      <meshStandardMaterial
        attach="material"
        color={ball.color}
        roughness={0.2}
        map={ballTexture}
        transparent={true}
        opacity={0.7}
      />
    </mesh>
  );
}
