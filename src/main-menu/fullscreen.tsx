import NoSleep from 'nosleep.js';

// Note: screen.orientation.lock() throws an error in the developer tools, but works in a browser on a physical mobile.
export async function enterFullscreen() {
  try {
    new NoSleep().enable();
    await document.documentElement.requestFullscreen();
    await window.screen.orientation.lock("landscape-primary");
  } catch (err) {
    console.warn(err);
  }
}

export async function exitFullscreen() {
  try {
    await document.exitFullscreen();
    window.screen.orientation.unlock();
    new NoSleep().disable();
  } catch (err) {
    console.warn(err);
  }
}
