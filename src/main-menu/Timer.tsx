import './Timer.scss';
import React from 'react';
import { useStore } from '../model/store';
import { Screen } from '../model/types';

// Note: This has to be separate from other components, since it changes each frame.
export function Timer() {
  const screen = useStore(store => store.screen);
  const levelTime = useStore(store => store.levelTime);
  const bestTime = useStore(store => store.bestTime);

  // 94 si the circle's circumference; calculated from its radius defined in the svg.
  const strokeDashoffset = 94.245 * Math.min(levelTime / bestTime, 1);

  return screen === Screen.Playing ? (
    <div className="timer">

      <div className={'star' +  (levelTime >= bestTime ? ' vanished' : '')}>
        ☆
      </div>

      <svg height="32" width="32">
        <circle cx="16" cy="16" r="15" style={{ strokeDashoffset }} />
      </svg>

    </div>
  ) : <></>;
}
