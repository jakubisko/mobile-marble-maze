import './MenuContent.scss';
import React, { useState } from 'react';
import { useStore } from '../model/store';
import { Screen, initialBestTimes } from '../model/types';
import { enterFullscreen, exitFullscreen } from './fullscreen';
import { levels } from '../model/levelDefinition';

function timeToString(time: number): string {
  time = Math.floor(time / 1000);
  return Math.floor(time / 60) + ':' + (time % 60 < 10 ? '0' : '') + (time % 60);
}


function IntroSubscreen() {
  const loadLevel = useStore(store => store.loadLevel);

  return (
    <div>
      <h1>Marble Maze</h1>

      <button
        className="highlight"
        style={{ position: 'absolute', bottom: 20 }}
        onClick={() => { enterFullscreen(); loadLevel(1); }}
      >
        Start
      </button>
    </div>
  );
}

function GetReadySubscreen() {
  return (
    <div className="get-ready">
      <h1 className="zoom-out">Get ready!</h1>
    </div>
  );
}


function ListOfLevelsSubpage(props: { onBack: () => void }) {
  const loadLevel = useStore(store => store.loadLevel);
  const localStorage = useStore(store => store.localStorage);

  const bestTimes = (localStorage.readFromStorage() ?? initialBestTimes);

  return (
    <div>
      <h1>List of levels</h1>

      <div className="list-of-levels">
        {levels.map((level, id) => id).slice(1).map(id => (
          <button
            key={id}
            onClick={() => loadLevel(id)}
          >
            <div>{id}</div>
            <div>{timeToString(bestTimes[id])}</div>
          </button>
        ))}
      </div>

      <button onClick={() => props.onBack()}> Back </button>
    </div>
  );
}


function Times() {
  const levelTime = useStore(store => store.levelTime);
  const bestTime = useStore(store => store.bestTime);
  const screen = useStore(store => store.screen);

  return (
    <div className="times">
      <span> Time: {timeToString(levelTime)} </span>

      {levelTime <= bestTime && screen === Screen.LevelWon && (
        <span className="zoom-out"> New best time! </span>
      )}

      <span> Best: {timeToString(bestTime)} </span>
    </div>
  );
}


export function MenuContent() {
  const [listOfLevels, setListOfLevels] = useState(false);

  const currentLevel = useStore(store => store.currentLevel);
  const screen = useStore(store => store.screen);
  const showScreen = useStore(store => store.showScreen);
  const loadLevel = useStore(store => store.loadLevel);
  const wasFullscreenOnStart = useStore(store => store.wasFullscreenOnStart);

  if (screen === Screen.Intro) {
    return <IntroSubscreen />
  } else if (screen === Screen.GetReady) {
    return <GetReadySubscreen />
  } else if (listOfLevels) {
    return <ListOfLevelsSubpage onBack={() => setListOfLevels(false)} />
  }

  return (
    <div>
      {screen === Screen.Menu && <h1>Menu</h1>}
      {screen === Screen.LevelWon && <h1 className="zoom-out">Cleared</h1>}
      {screen === Screen.LevelLost && <h1 className="zoom-out">Failed</h1>}

      <Times />

      {screen === Screen.LevelWon ? (
        <button
          className={currentLevel + 1 >= levels.length ? '' : 'highlight'}
          disabled={currentLevel + 1 >= levels.length}
          onClick={() => loadLevel(currentLevel + 1)}
        >
          Next level
        </button>
      ) : (
        <button
          disabled={screen === Screen.LevelLost}
          onClick={() => showScreen(Screen.Playing)}
        >
          Resume
        </button>
      )}

      <button
        className={screen === Screen.LevelLost ? 'highlight' : ''}
        onClick={() => loadLevel(currentLevel)}
      >
        Restart
      </button>

      <button onClick={() => setListOfLevels(true)}> List of levels </button>

      {wasFullscreenOnStart ?
        <button onClick={() => window.close()}> Exit game </button>
        :
        <button onClick={() => { loadLevel(0); exitFullscreen(); }}> Exit fullscreen </button>
      }

    </div >
  );
}
