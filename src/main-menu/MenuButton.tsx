import './MenuButton.scss';
import React from 'react';
import { useStore } from '../model/store';
import { Screen } from '../model/types';
import Modal from 'react-modal';
import { MenuContent } from './MenuContent';
import { Timer } from './Timer';

export function MenuButton() {
  const screen = useStore(store => store.screen);
  const showScreen = useStore(store => store.showScreen);

  return screen === Screen.Playing ? (
    <>
      <button className="menu-button" onClick={() => showScreen(Screen.Menu)}>
        <div className="hamburger-symbol" />
      </button>
      <Timer />
    </>
  ) : (
    <Modal
      isOpen={true}
      className="menu-modal"
      overlayClassName="menu-modal-overlay"
    >
      <MenuContent />
    </Modal>
  );
}
