import React from 'react';
import { GameCanvas } from './gameui/GameCanvas';
import { MenuButton } from './main-menu/MenuButton';
import Modal from 'react-modal';

function App() {
  Modal.setAppElement('#app');

  return (
    <div id="menu-blocker" onContextMenu={e => e.preventDefault()}>
      <MenuButton />
      <GameCanvas />
    </div>
  );
}

export default App;
