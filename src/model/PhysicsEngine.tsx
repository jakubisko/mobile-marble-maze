import { MazeElement, MazeDefinition, mazeWidth, mazeHeight } from './levelDefinition';
import { DeviceOrientationState } from '../gameui/useDeviceOrientation';
import { DeviceMotionState } from '../gameui/useDeviceMotion';
import { BallData, MutableState } from './types';

/**
 * Balls move in discrete steps. If a ball moved fast while the FPS was low, the steps would get so high that the ball would jump
 * over the wall. To prevent this, we instead do multiple smaller steps per each frame.
 */
const physicsEngineUpdatesPerFrame = 5;

// Must be larger than ballRadius.
export const holeRadius = 0.95;

// Should be less than holeRadius.
export const ballRadius = 0.9;

/** Higher value means that the ball falls on a Z-axis into a hole faster. */
const holeDownwardAcceleration = 0.125;

/** Higher value means that the ball changed direction around the hole faster. */
const holeCentripetalAcceleration = 50;

/** Higher value means that the ball loses more energy when it rises from the hole. */
const holeUpwardsFriction = 60000;

/** Higher value means that balls speed up due to the phone tilting faster. */
const slopeAcceleration = 70;

/** Higher value means that balls speed up due to the phone shaking faster. */
const motionAcceleration = 40;

/** Ball deacceleration caused by the friction. */
const deskFriction = 0.3;

/**
 * When ball rebounds from a wall, it loses a fraction of its speed. The fraction depends on the angle.
 * This is the largest fraction of the speed that can be lost - in direct hit. The loss nears zero as the angle with the wall nears zero.
 */
const directWallHitSpeedLossCoef = 0.5;

/** Rebounding is calculated by checking several points around the ball each frame. */
const reboundSamples = 92;

export class PhysicsEngine {

  public constructor(private mazeDefinition: MazeDefinition, private mutable: MutableState) {
  }

  private applySensorInputs(ball: BallData, deltaTime: number, deviceOrientation: DeviceOrientationState, deviceMotion: DeviceMotionState) {
    ball.speedX += deltaTime * (
      slopeAcceleration * Math.sin(deviceOrientation.beta * Math.PI / 180)
      + motionAcceleration * deviceMotion.y * Math.cos(deviceOrientation.beta * Math.PI / 180)
      + motionAcceleration * deviceMotion.z * Math.sin(deviceOrientation.beta * Math.PI / 180)
    );
    ball.speedY += deltaTime * (
      slopeAcceleration * Math.sin(deviceOrientation.gamma * Math.PI / 180)
      - motionAcceleration * deviceMotion.x * Math.cos(deviceOrientation.gamma * Math.PI / 180)
      - motionAcceleration * deviceMotion.z * Math.sin(deviceOrientation.gamma * Math.PI / 180)
    );
  }

  // If the distance between the ball and the nearest hole is larger than the hole's radius, arbitrary larger offset is returned.
  private calculateOffsetTowardsNearestHole(ball: BallData, includingFinish: boolean): { offsetX: number, offsetY: number } {
    const { x, y } = ball;

    let nearestX = 2 * holeRadius;
    let nearestY = 2 * holeRadius;

    const x0 = Math.floor(x);
    const y0 = Math.floor(y);
    for (let xx = x0 - 1; xx <= x0 + 1; xx++) {
      for (let yy = y0 - 1; yy <= y0 + 1; yy++) {
        const element = this.mazeDefinition[yy][xx];
        if (element === MazeElement.Hole || (includingFinish && element === MazeElement.Finish)) {
          const offsetX = x - 0.5 - xx;
          const offsetY = y - 0.5 - yy;
          if (offsetX * offsetX + offsetY * offsetY < nearestX * nearestX + nearestY * nearestY) {
            nearestX = offsetX;
            nearestY = offsetY;
          }
        }
      }
    }

    return { offsetX: nearestX, offsetY: nearestY };
  }

  // Given a distance from the hole center, this returns how low the ball can fall into the hole at that position.
  private minPossibleZ(dist: number): number {
    const distToEdge = holeRadius - dist;
    if (distToEdge < 0) {
      return 0;
    } else if (distToEdge >= ballRadius) {
      return -ballRadius;
    } else {
      return Math.sqrt(ballRadius * ballRadius - distToEdge * distToEdge) - ballRadius;
    }
  }

  private applyHoleCentripetalForce(ball: BallData, deltaTime: number): void {
    let { offsetX, offsetY } = this.calculateOffsetTowardsNearestHole(ball, true);

    const dist = Math.sqrt(offsetX * offsetX + offsetY * offsetY);
    if (dist < holeRadius) {
      // Normalization of the vector towards the center of the hole.
      offsetX /= dist;
      offsetY /= dist;

      // Centripetal acceleration. This causes the ball to change direction when it just "brushes" the hole.
      const holeAttraction = holeCentripetalAcceleration * deltaTime;
      ball.speedX -= holeAttraction * offsetX;
      ball.speedY -= holeAttraction * offsetY;

      // If the ball is at least partially over the hole, it falls with a free fall into it.
      const lastZ = ball.z;

      ball.speedZ -= holeDownwardAcceleration * deltaTime;
      ball.z += ball.speedZ;
      const minPossibleZ = this.minPossibleZ(dist);
      if (ball.z < minPossibleZ) {
        ball.z = minPossibleZ;
        ball.speedZ = 0;
      }

      // When the ball is rising from the hole, its speed decreases because that costs extra energy.
      this.applyFriction(ball, deltaTime, Math.max(holeUpwardsFriction * (ball.z - lastZ), 0));

      // If the ball is already at the bottom of the hole, stop it so it doesn't oscillate around the center.
      if (minPossibleZ === -ballRadius) {
        ball.speedX = 0;
        ball.speedY = 0;
      }

    } else {
      ball.z = 0;
    }
  }

  // Returns the angles at which the ball would touch a wall if it stood at given position..
  // Zero items are returned if the ball wouldn't touch a wall.
  // Multiple items are returned in case of a corner.
  private calculateAnglesTowardsTouchedWall(x: number, y: number): number[] {
    const wallAtDirection = new Array<boolean>(reboundSamples + 1);
    wallAtDirection[reboundSamples] = false;

    // Make samples around the ball to find out angles at which the ball touches the wall.
    for (let i = 0; i < reboundSamples; i++) {
      const a = i * 2 * Math.PI / reboundSamples;
      const x2 = x + 0.5 + Math.cos(a) * ballRadius;
      const y2 = y + 0.5 + Math.sin(a) * ballRadius;
      wallAtDirection[i] = x2 < 0 || x2 >= mazeWidth || y2 < 0 || y2 >= mazeHeight || this.mazeDefinition[Math.floor(y2)][Math.floor(x2)] === MazeElement.Wall;
    }

    // Find continuous intervals of the angles.
    const intervals: { first: number, last: number }[] = [];
    let start = 0;
    let searchingForStart = !wallAtDirection[0];

    for (let i = 1; i <= reboundSamples; i++) {
      if (wallAtDirection[i] === searchingForStart) {
        if (searchingForStart) {
          start = i;
          searchingForStart = false;
        } else {
          intervals.push({ first: start, last: i - 1 });
          searchingForStart = true;
        }
      }
    }

    // Special case - if there are intervals 0-20 and 350-359, they actually repesent a continuous interval (-10)-20.
    if (intervals.length > 1 && intervals[0].first === 0 && intervals[intervals.length - 1].last === reboundSamples - 1) {
      intervals[0].first = intervals.pop()!.first - reboundSamples;
    }

    // Take the middle of each interval as the angle representing the normal vector for that surface.
    // If there are two surfaces, such as the corner, then their average vector represents the final normal vector used for the bounce.
    return intervals.map(interval => (interval.first + interval.last) * Math.PI / reboundSamples);
  }

  private modulo(num: number, by: number): number {
    return ((num % by) + by) % by;
  }

  private bounceFromWalls(ball: BallData, anglesTowardsTouchedWalls: number[]): void {
    // If the ball would touch multiple walls, bounce from them one at a time.
    for (let angleTowardsWall of anglesTowardsTouchedWalls) {

      // Calculate the angle of the ball movement before and after the hit.
      const angleIncoming = Math.atan2(ball.speedY, ball.speedX);
      const angleOutgoing = Math.PI + 2 * angleTowardsWall - angleIncoming;

      let angleChange = this.modulo(angleIncoming - angleOutgoing, 2 * Math.PI);
      if (angleChange > Math.PI) {
        angleChange = 2 * Math.PI - angleChange;
      }

      const speedFractionRetained = 1 - Math.sin(angleChange / 2) * directWallHitSpeedLossCoef;
      const speed = Math.sqrt(ball.speedX * ball.speedX + ball.speedY * ball.speedY) * speedFractionRetained;
      ball.speedX = Math.cos(angleOutgoing) * speed;
      ball.speedY = Math.sin(angleOutgoing) * speed;
    }
  }

  private applyFriction(ball: BallData, deltaTime: number, friction: number): void {
    const { speedX, speedY } = ball;

    const speed = Math.sqrt(speedX * speedX + speedY * speedY);
    const speedNew = speed - friction * deltaTime;

    if (speedNew > 0) {
      ball.speedX *= speedNew / speed;
      ball.speedY *= speedNew / speed;
    } else {
      ball.speedX = 0;
      ball.speedY = 0;
    }
  }

  private bounceTwoBalls(ball1: BallData, ball2: BallData): void {
    // Algorithm using complex numbers for ellastic collision found online.
    const dx = ball1.x - ball2.x;
    const dy = ball1.y - ball2.y;

    const real = ((ball1.speedX - ball2.speedX) * dx + (ball1.speedY - ball2.speedY) * dy) / (dx * dx + dy * dy);

    ball1.speedX -= dx * real;
    ball1.speedY -= dy * real;
    ball2.speedX += dx * real;
    ball2.speedY += dy * real;
  }

  /** moveAfterBouncingFromWall is to prevent infinite recursion. */
  private tryMovingBallTo(ball: BallData, x: number, y: number, moveAfterBouncingFromWall: boolean): void {
    for (const ball2 of this.mutable.balls) {
      const x2 = ball2.x;
      const y2 = ball2.y;
      if (ball !== ball2 && Math.sqrt((x - x2) * (x - x2) + (y - y2) * (y - y2)) <= 2 * ballRadius) {
        this.bounceTwoBalls(ball, ball2);
        return;
      }
    }

    const anglesTowardsTouchedWalls = this.calculateAnglesTowardsTouchedWall(x, y);
    if (anglesTowardsTouchedWalls.length > 0) {
      this.bounceFromWalls(ball, anglesTowardsTouchedWalls);

      if (moveAfterBouncingFromWall) {
        // After the ball bounces from a wall, we must push it away from the wall.
        // Otherwise it used to get "stuck" to a wall when the ball's speed is less than the gravity.
        // Push it away a little bit in a direction directly opposite the wall.

        let x1 = ball.x;
        let y1 = ball.y;
        for (let anglesTowardsTouchedWall of anglesTowardsTouchedWalls) {
          x1 -= Math.cos(anglesTowardsTouchedWall) * 0.001;
          y1 -= Math.sin(anglesTowardsTouchedWall) * 0.001;
        }

        this.tryMovingBallTo(ball, x1, y1, false);
      }

      return;
    }

    ball.x = x;
    ball.y = y;
  }

  public update(deltaTime: number, deviceOrientation: DeviceOrientationState, deviceMotion: DeviceMotionState): void {
    deltaTime /= physicsEngineUpdatesPerFrame;
    for (let f = 0; f < physicsEngineUpdatesPerFrame; f++) {
      for (const ball of this.mutable.balls) {
        this.applySensorInputs(ball, deltaTime, deviceOrientation, deviceMotion);

        this.applyHoleCentripetalForce(ball, deltaTime);

        this.applyFriction(ball, deltaTime, deskFriction);

        const x1 = ball.x + ball.speedX * deltaTime;
        const y1 = ball.y + ball.speedY * deltaTime;
        this.tryMovingBallTo(ball, x1, y1, true);
      }
    }
  }

  public numberOfBallsInHoles(includingFinish: boolean): number {
    let result = 0;

    for (const ball of this.mutable.balls) {
      if (ball.z <= -ballRadius) {
        const { offsetX, offsetY } = this.calculateOffsetTowardsNearestHole(ball, includingFinish);
        if (Math.sqrt(offsetX * offsetX + offsetY * offsetY) < holeRadius * holeRadius) {
          result++;
        }
      }
    }

    return result;
  }

}
