import { State } from 'zustand';
import { Geometry } from 'three';
import { PhysicsEngine } from './PhysicsEngine';
import { LocalStorage } from './LocalStorage';
import { levels } from './levelDefinition';
import { DeviceOrientationState } from '../gameui/useDeviceOrientation';

/** Increase this number whenever a structure of the persistent type "BestTimes" changes. */
export const LOCAL_STORAGE_KEY = 'MOBILE_MARBLE_MAZE_BEST_TIMES';
export const currentBestTimesVersion = 2;
export type BestTimes = number[];
// [0] is intro
export const initialBestTimes: BestTimes = levels.map(level => 9 * 60 * 1000 + 59 * 1000);

export interface GlobalState extends State {
  // State inside the React lifecycle. If you change one of these values, React will redraw components that use it.
  wasFullscreenOnStart: boolean; // True if the application was started as a standalone program. False when opened as a tab in a browser.
  localStorage: LocalStorage<BestTimes>,
  simulatedDeviceOrientation?: DeviceOrientationState; // On a computer, you can simulate the device orientation with mouse dragging.
  physicsEngine: PhysicsEngine;
  deskGeometry: Geometry;
  wallGeometry: Geometry;
  screen: Screen;
  currentLevel: number;
  levelTime: number; // Time displayed to the user.
  bestTime: number; // Best time for current level.

  mutable: MutableState;

  // Actions on state in react lifecycle.
  setSimulatedDeviceOrientation: (simulatedDeviceOrientation: DeviceOrientationState) => void;
  showScreen: (screen: Screen) => void;
  updateBestTimes: () => void;
  increaseLevelTime: (deltaTime: number) => void;
  loadLevel: (level: number) => void;
}

/**
 * These variables change every frame, so we place them outside of the React lifecycle for performance reasons.
 * Change fields inside this object directly, so that the React won't notice it, rather than replacing the object with a new state.
 */
export interface MutableState {
  lastTime: number; // Time for animation purposes.
  balls: BallData[];
  finishes: FinishData[];
}

export const initialMutableStateState: MutableState = {
  lastTime: 0,
  balls: [],
  finishes: []
}

export interface BallData {
  color: string;
  x: number;
  y: number;
  z: number; // Positive is above the desk, negative is in a hole.
  speedX: number;
  speedY: number;
  speedZ: number;
}

export interface FinishData {
  x: number;
  y: number;
}

export enum Screen {
  Playing,
  GetReady,
  LevelWon,
  LevelLost,
  Intro,
  Menu
}
