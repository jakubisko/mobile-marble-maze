import create from 'zustand';
import { mazeWidth, mazeHeight, MazeElement, MazeDefinition, levels } from './levelDefinition';
import { CylinderGeometry, Geometry, PlaneGeometry, Vector2 } from 'three';
import ThreeBSP from 'three-solid';
import { holeRadius, PhysicsEngine } from './PhysicsEngine';
import { GlobalState, MutableState, initialMutableStateState, BallData, FinishData, Screen, BestTimes, currentBestTimesVersion, LOCAL_STORAGE_KEY, initialBestTimes } from './types';
import { LocalStorage } from './LocalStorage';
import { DeviceOrientationState } from '../gameui/useDeviceOrientation';

const ballColors = ['green', 'deepskyblue', 'gold', 'red'];

export const useStore = create<GlobalState>((set, get) => {
  return {
    wasFullscreenOnStart: window.matchMedia('(display-mode: fullscreen)').matches,
    localStorage: new LocalStorage<BestTimes>(LOCAL_STORAGE_KEY, currentBestTimesVersion),
    physicsEngine: new PhysicsEngine([], initialMutableStateState),
    deskGeometry: new Geometry(),
    wallGeometry: new Geometry(),
    screen: Screen.Intro,
    currentLevel: 1,
    levelTime: 0,
    bestTime: 1,

    mutable: initialMutableStateState,

    setSimulatedDeviceOrientation: (simulatedDeviceOrientation: DeviceOrientationState) => {
      set({ simulatedDeviceOrientation });
    },

    showScreen: (screen: Screen) => {
      set({ screen });
    },

    updateBestTimes: () => {
      const { localStorage, levelTime, currentLevel, bestTime } = get();

      if (levelTime < bestTime) {
        const bestTimes = (localStorage.readFromStorage() ?? [...initialBestTimes]);
        bestTimes[currentLevel] = levelTime;
        localStorage.writeToStorage(bestTimes);
        set({ bestTime: levelTime });
      }
    },

    increaseLevelTime: (deltaTime: number) => {
      set(state => ({
        levelTime: state.levelTime + deltaTime
      }));
    },

    loadLevel: (level: number) => {
      const { localStorage, deskGeometry, wallGeometry, currentLevel } = get();
      const mazeDefinition = [...levels[level]].reverse();

      const mutable: MutableState = {
        lastTime: Date.now(),
        balls: findBalls(mazeDefinition),
        finishes: findFinishes(mazeDefinition)
      };

      // Calculate the geometries only if loading another level. It's the most expensive operation.
      if (currentLevel !== level) {
        deskGeometry.dispose();
        wallGeometry.dispose();
        set({
          deskGeometry: calculateDeskGeometry(mazeDefinition),
          wallGeometry: calculateWallGeometry(mazeDefinition)
        });
      }

      set({
        simulatedDeviceOrientation: undefined,
        physicsEngine: new PhysicsEngine(mazeDefinition, mutable),
        screen: level === 0 ? Screen.Intro : Screen.GetReady,
        currentLevel: level,
        levelTime: 0,
        bestTime: (localStorage.readFromStorage() ?? initialBestTimes)[level],
        mutable
      });

      // After the level is loaded, display "Get ready" text, wait a while and only then starting playing.
      if (level > 0) {
        setTimeout(() => set({
          screen: Screen.Playing
        }), 1000);
      }
    }
  }
});

function findBalls(mazeDefinition: MazeDefinition): BallData[] {
  const result: BallData[] = [];

  for (let y = 0; y < mazeHeight; y++) {
    for (let x = 0; x < mazeWidth; x++) {
      if (mazeDefinition[y][x] === MazeElement.Ball) {
        result.push({
          color: ballColors[result.length % ballColors.length],
          x: x + 0.5,
          y: y + 0.5,
          z: 0,
          speedX: 0,
          speedY: 0,
          speedZ: 0,
        });
      }
    }
  }

  return result;
}

function findFinishes(mazeDefinition: MazeDefinition): FinishData[] {
  const result: FinishData[] = [];

  for (let y = 0; y < mazeHeight; y++) {
    for (let x = 0; x < mazeWidth; x++) {
      if (mazeDefinition[y][x] === MazeElement.Finish) {
        result.push({
          x: x + 0.5,
          y: y + 0.5
        });
      }
    }
  }

  return result;
}

function calculateWallGeometry(mazeDefinition: MazeDefinition): Geometry {
  const resultGeometry = new Geometry();

  for (let y = 0; y < mazeHeight; y++) {
    for (let x = 0; x < mazeWidth; x++) {
      if (mazeDefinition[y][x] === MazeElement.Wall) {
        // Front
        const planeGeometry = createPartOfDesk((y + 20) % mazeHeight, (x + 30) % mazeWidth, 1);
        planeGeometry.translate(x - (mazeWidth - 1) / 2, y - (mazeHeight - 1) / 2, 0);
        resultGeometry.merge(planeGeometry);
        planeGeometry.dispose();

        // Right
        if (x < mazeWidth - 1 && mazeDefinition[y][x + 1] !== MazeElement.Wall) {
          const planeGeometry = new PlaneGeometry(1, 1);
          planeGeometry.rotateY(Math.PI / 2);
          planeGeometry.translate(x - (mazeWidth - 1) / 2 + 0.5, y - (mazeHeight - 1) / 2, -0.5);
          resultGeometry.merge(planeGeometry);
          planeGeometry.dispose();
        }

        // Left
        if (x > 0 && mazeDefinition[y][x - 1] !== MazeElement.Wall) {
          const planeGeometry = new PlaneGeometry(1, 1);
          planeGeometry.rotateX(Math.PI);
          planeGeometry.rotateY(Math.PI / 2);
          planeGeometry.translate(x - (mazeWidth - 1) / 2 - 0.5, y - (mazeHeight - 1) / 2, -0.5);
          resultGeometry.merge(planeGeometry);
          planeGeometry.dispose();
        }

        // Top
        if (y < mazeHeight - 1 && mazeDefinition[y + 1][x] !== MazeElement.Wall) {
          const planeGeometry = new PlaneGeometry(1, 1);
          planeGeometry.rotateX(-Math.PI / 2);
          planeGeometry.translate(x - (mazeWidth - 1) / 2, y - (mazeHeight - 1) / 2 + 0.5, -0.5);
          resultGeometry.merge(planeGeometry);
          planeGeometry.dispose();
        }

        // Bottom
        if (y > 0 && mazeDefinition[y - 1][x] !== MazeElement.Wall) {
          const planeGeometry = new PlaneGeometry(1, 1);
          planeGeometry.rotateX(Math.PI / 2);
          planeGeometry.translate(x - (mazeWidth - 1) / 2, y - (mazeHeight - 1) / 2 - 0.5, -0.5);
          resultGeometry.merge(planeGeometry);
          planeGeometry.dispose();
        }
      }
    }
  }

  return resultGeometry;
}

function updateTextureParams(plane: Geometry, sMin: number, sMax: number, tMin: number, tMax: number): void {
  const [face0, face1] = plane.faceVertexUvs[0];
  face0[0] = new Vector2(sMin, tMax);
  face0[1] = new Vector2(sMin, tMin);
  face0[2] = new Vector2(sMax, tMax);
  face1[0] = new Vector2(sMin, tMin);
  face1[1] = new Vector2(sMax, tMin);
  face1[2] = new Vector2(sMax, tMax);
  plane.uvsNeedUpdate = true;
}

function createPartOfDesk(y: number, x: number, tileSize: number): Geometry {
  const planeGeometry = new PlaneGeometry(tileSize, tileSize);
  const sMin = x / mazeWidth;
  const sMax = (x + tileSize) / mazeWidth;
  const tMin = y / mazeHeight;
  const tMax = (y + tileSize) / mazeHeight;
  updateTextureParams(planeGeometry, sMin, sMax, tMin, tMax);
  return planeGeometry;
}

function calculateDeskGeometry(mazeDefinition: MazeDefinition): Geometry {
  const isFree = (y: number, x: number) => mazeDefinition[y][x] === MazeElement.Empty || mazeDefinition[y][x] === MazeElement.Ball;
  const isHole = (y: number, x: number) => x >= 0 && y >= 0 && (mazeDefinition[y][x] === MazeElement.Hole || mazeDefinition[y][x] === MazeElement.Finish);

  let resultGeometry = new Geometry();

  for (let y = 0; y < mazeHeight; y++) {
    for (let x = 0; x < mazeWidth; x++) {
      if (isFree(y, x) && !isHole(y - 1, x) && !isHole(y, x - 1) && !isHole(y - 1, x - 1)) {
        const planeGeometry = createPartOfDesk(y, x, 1);
        planeGeometry.translate(x - (mazeWidth - 1) / 2, y - (mazeHeight - 1) / 2, 0);

        resultGeometry.merge(planeGeometry);
        planeGeometry.dispose();
      } else if (isHole(y, x)) {
        const planeGeometry = createPartOfDesk(y, x, 2);
        const holeGeometry = new CylinderGeometry(holeRadius, holeRadius, 2, 16);
        holeGeometry.rotateX(-Math.PI / 2);

        const planeWithHoleBsp = (new ThreeBSP(planeGeometry)).subtract(new ThreeBSP(holeGeometry));
        const { geometry } = planeWithHoleBsp.toMesh(null);
        geometry.translate(x + 1 - mazeWidth / 2, y + 1 - mazeHeight / 2, 0);
        resultGeometry.merge(geometry);

        planeGeometry.dispose();
        holeGeometry.dispose();
      }
    }
  }

  return resultGeometry;
}
