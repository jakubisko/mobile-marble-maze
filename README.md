# Marble Maze

Puzzle game controlled by tilting the phone.

### npm start

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

### npm run build

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
Your app is ready to be deployed!

### firebase deploy

Deploys the production build to Firebase server.

### React / Three.JS game example with source code

https://codesandbox.io/embed/r3f-game-i2160

### How to test device orientation in Chrome developer tools:

"..." > More Tools > Sensors

# Libraries

* three.js - JavaScript 3D library.
  https://www.npmjs.com/package/three

* react-three-fiber - Declare Three.js objects using React syntax.
  https://www.npmjs.com/package/react-three-fiber

* Three Solid / ThreeCSG - Calculate union, difference and intersection of geometries; for example to create holes into an object.
  https://github.com/chandlerprall/ThreeCSG

* zustand - Small, fast and scaleable bearbones state-management solution. Faster than Redux.
  https://github.com/react-spring/zustand

* react-modal - Modal window.
  https://www.npmjs.com/package/react-modal

* NoSleep.js - Prevent sleep of the display on the mobile.
  https://www.npmjs.com/package/nosleep.js

* ScreenOrientation API - Lock screen to prevent it from flipping.
  https://www.w3.org/TR/screen-orientation/
  